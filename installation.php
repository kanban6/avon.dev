<?php

require_once 'system/autoloader.php';
use system\Config;

if (php_sapi_name() === 'cli') {

   $dbInstall = new install();

   $dbInstall->createDatabase();

   exit();
}


/**
 * This class establishes database
 */
class install
{
	/**
	 * [$mysqli connect to database]
	 * @var [object]
	 */
	private $mysqli;

	/**
	 * [$db config database]
	 * @var [array]
	 */
	private $db;

	/**
	 * [$tables database tables]
	 * @var [array]
	 */
	private $tables;

	/**
	 * [__construct get config to database and connect]
	 */
	public function __construct()
	{

		$this->db = config::getConfig('database');

   		$this->mysqli = new \mysqli ($this->db['host'], $this->db['username'], $this->db['password']);

   		$this->tables = Config::getConfig('databaseTable');	

	}

	/**
	 * [createDatabase this function create database]
	 */
	public function createDatabase()
	{
		
   		if ($this->mysqli->connect_errno !== 0) {

   			echo $this->mysqli->connect_errno . PHP_EOL;

   		} else {

   			echo 'Connection to MySQL established' . PHP_EOL;

   			if ($this->mysqli->select_db($this->db['database']) === false) {

   		 		if ($this->mysqli->query('CREATE DATABASE'.' '.$this->db['database']) === true) {

                	echo 'Database created' . PHP_EOL;

                	$this->mysqli->select_db($this->db['database']);

                	$this->createTable();

         		} else {

                	echo 'Database can not be created';
            	}

   			} else {

           		echo 'Database already created' . PHP_EOL;
       		}

  		}

	}
	

	/**
	 * [createTable this function create table with database]
	 */
	public function createTable()
	{
	
		foreach ($this->tables as $table => $values) {
	
			$sql = 'create table ' . $table  . ' (';

			foreach ($values as $fild => $value) {
				
					$filds[]= $fild . ' ' . $value;

			}

			$sql .= implode(', ', $filds) . ')';

			unset($filds);

			if ($this->mysqli->query($sql) === true) {

        		echo 'Teble ' . $table . ' created ' . PHP_EOL;

    		} else {

        		echo "Error created table " . $table . PHP_EOL . $this->mysqli->error . PHP_EOL;
    		}
		}

	}


}