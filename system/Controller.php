<?php

namespace system;

/**
 * class Controller
 * control view and model
 */
class Controller
{
	
	/**
	 * getView include view file and layout file
	 * @param  string $view   | name view file
	 * @param  string $layout | name layout file
	 */
	public static function View($view, $layout = 'layout') 
	{
		
		$layoutFile = 'MVC/View/Layout/' . $layout . '.phtml';

		if (file_exists($layoutFile)) {

			ob_start();

	        include_once 'MVC/View/' . $view . '.phtml';

	        $content = ob_get_clean();

	        include_once $layoutFile;

		} else {

			include_once 'MVC/View/' . $view . '.phtml';
			
		}
		
	}

}