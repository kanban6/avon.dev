<?php

namespace system;

/**
* class File
* Upload and delete file
*/
class File
{

	/**
	 * $dir directory with files
	 * @var string
	 */
	protected $dir = 'img';

	/**
	 * $file $_FIFLE
	 * @var array
	 */
	protected $file;


	/**
	 * __construct create object
	 * @param array | $file $_FIFLE
	 */
	public function __construct($file)
	{
		$this->file = $file;
	}


	/**
	 * upload uploading file into server
	 * @return bool | string 
	 */
	public function upload()
	{
		$fileError = $this->file['error'];

		if ($fileError == 0) {

			$dir = $this->dir . "/" . $this->getName();
		
			if (file_exists($dir)) {

				$this->setName('Copy');
				$dir = $this->dir . "/" . $this->getName();
			}

			$result = move_uploaded_file($this->file['tmp_name'], $dir);
		   
			return $result;

		} else {

			return $file['error'];
		}
		
	}	


	/**
	 * upload uploading img file into server
	 * @return bool | string 
	 */
	public function uploadImg()
	{

		if ($this->getType() == 'image') {

			return $this->upload();

		} else {

			return false;
		}
	}


	/**
	 * delete delete file in the server
	 * @param  string | $name filename
	 * @return bool       
	 */
	public static function delete($name)
	{
		$result = unlink("$this->dir/$name");

		return $result;
	}


	/**
	 * getName get name this file
	 * @return string | this filename
	 */
	public function getName()
	{
		$name = basename($this->file['name']);
		return $name;

	}


	/**
	 * getType get type this file
	 * @return string | this file type
	 */
	public function getType()
	{

		$type = $this->file['type'];
		$type = explode('/', $type);
	
		return $type[0];
	}


	/**
	 * setName pass filename
	 * @param string  |  $name new filename
	 * @param boolean | $boll is false added to the filename $name else true filename = $name
	 */
	public function setName($name, $boll = false)
	{

		$newName = explode('.', $this->getName());

		if (!$boll) {
			
			$newName[0] .="($name)"; 

		}else {

			$newName[0] = $name;
		}

		$name = implode('.', $newName );

		$this->file['name'] = $name;
	}
}

