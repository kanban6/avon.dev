<?php

namespace system\database;

/**
 * Class Database extends DbConnect
 * pattern singleton
 */
class Database extends DbConnect
{
	
	/**
	 * query database
	 * @param  string $sql SQL 
	 * @return bool
	 */
	public function query($sql)
	{
		$result = $this->connect->query($sql);

		return $result;
	}

	/**
	 * getRow get row from database
	 * @param  string $sql SQL
	 * @return bool | object
	 */
	public function getRow($sql)
	{

		$result = $this->connect->query($sql);

		return $result;
	}


	/**
	 * delete in database
	 * @param string $table  | table name
	 * @param array  $values | collum => value
	 * @return bool
	 */ 
	public function delete($table, $values = [])
	{
		$sql = "DELETE FROM $table WHERE ";

		$sql .= $this->arrayToString($values, ' AND ');

		$result = $this->connect->query($sql);
		
		return $result;
	}


	/**
	 * insert in database
	 * @param  string $table  | table name
	 * @param  array  $values | collum => value
	 * @return bool
	 */
	public function insert($table, $values = [])
	{
		$sql = "INSERT INTO $table ";

		foreach ($values as $key => $value) {
			if ($value == null) {
				break;
			}
			$field[] = $key;
			$fieldValue[] = "'".$value."'";
			
		}

		$field = '(' . implode(', ', $field) . ')';
		$fieldValue = '(' . implode(', ', $fieldValue) . ')';

		$sql .= $field . ' VALUES ' . $fieldValue;
		$result = $this->connect->query($sql);
		
		return $result;
	}


	/**
	 * update in database
	 * @param  string $table  | table name 
	 * @param  array  $values | collum => value
	 * @param  array  $where  | collum => value
	 * @return bool
	 */
	public function update($table, $values = [], $where = [])
	{

		$sql = "UPDATE $table SET ";

		$fieldValue = $this->arrayToString($values, ", ");

		$fildWere = $this->arrayToString($where, ' AND ');

		$sql .= $fieldValue . ' WHERE ' . $fildWere;

		$result = $this->connect->query($sql);

		return $result;	
	}


	/**
	 * arrayToString converted array to string
	 * @param  array  $arrayVal 
	 * @param  string $implode  
	 * @return string
	 */
	public function arrayToString($arrayVal = [], $implode)
	{

		foreach ($arrayVal as $key => $value) {
			
			$fieldValue[] = "$key = '$value'";
		}

		$fieldValue = implode($implode, $fieldValue);

		return $fieldValue;
	}

}

