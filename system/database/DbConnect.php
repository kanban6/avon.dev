<?php

namespace system\database;

use system\Config;

/**
 * Class dbConnect
 * Pattern singleton
 */
class dbConnect
{
    /**
     * $instance object
     * @var null
     */
    private static $instance = null;

    /**
     * $connect connect database
     * @var object
     */
    protected $connect;
    
    /**
     * getInstance create or return object
     * @return object | this object
     */
    public static function getInstance()
    {
        if (self::$instance === null) {

            $db = Config::getConfig('database');

            self::$instance = new static();
            self::$instance->connect = new \mysqli ($db['host'], $db['username'], $db['password'], $db['database']);
        }
        
        return self::$instance;
    }

    /**
     * getConnect connect to database
     * @return object | object mysqli
     */
    public function getConnect()
    {
        return $this->connect;
    }

    private function __clone() {}
    private function __construct() {}

    /**
     * __destruct close connect database
     */
    public function __destruct ()
    {

        $this->connect->close();
    }
    

}