<?php

/**
 * array config database
 */
return [
    'host'     => '127.0.0.1',
    'port'     => '3306',
    'username' => 'root',
    'password' => '',
    'database' => 'tovar',
    'charset'  => 'UTF-8'
];