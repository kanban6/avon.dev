<?php

/**
 * array with url route
 */
return [

    'urls' => [
        ''        => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'home'
        ],
        'registr' => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'registr'
        ],
        'contact' => [
            'controller' => 'MVC\Controller\Pages',
            'action'     => 'contact'
        ],
        'admin' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'login'
        ],
        'admin/home' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'home'
        ],
        'admin/logout' => [
            'controller' => 'MVC\Controller\Admin',
            'action'     => 'logout'
        ]    
   
    ],

    'pattern' => [

        'tovar\/[0-9]+' => [
            'controller' => 'MVC\Controller\Tovars',
            'action'     => 'tovar'
        ]
 
    ]
  
];