<?php

/**
 * array tables databese table=>field=>type
 */
return [

	'comment' => [

			'id'    => 'int(6) AUTO_INCREMENT KEY',
			'title' => 'varchar(150) NOT NULL'
	],	

	'users'   => [

			'id'    => 'int(1) AUTO_INCREMENT KEY NOT NULL',
			'name'  => 'varchar(200) NOT NULL',
			'family'  => 'varchar(200) NOT NULL'

	]


];