<?php

namespace MVC\Controller;

use system\Controller;
use system\Router;
use MVC\Model\Admin;

/**
* 
*/
class AdminController extends Controller
{
	private $admin;
	private $status;

	function __construct()
	{
		
		$url = Router::getUrl();
		$this->admin = Admin::getInstance();
		
		$this->status = $this->admin->logStatus();
		
		if (!$this->status && $url != 'admin') {

				header("Location: /admin");	
				die('You are not logged in');
		}
		
	}


	public function loginAction()
	{
		$this->View('adminLogin');

		$this->admin->setSession();

	}


	public function homeAction()
	{
		$this->View('adminHome');
	}

	public function logoutAction()
	{
		$this->admin->clearSession();
		header("Location: /admin");	
	}
}