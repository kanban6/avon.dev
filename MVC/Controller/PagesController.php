<?php

namespace MVC\Controller;

use system\Controller;

class PagesController extends Controller
{
	
	public function homeAction()
	{
		$this->View('home');
	}

	public function registrAction()
	{
		$this->View('home', 'shopLayout');
	}

	public function contactAction ()
	{
		$this->View('home', false);
	}
}