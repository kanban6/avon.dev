<?php

namespace MVC\Model;

use system\session\Session;

use system\database\Database;

/**
* 
*/
class Admin 
{


	/**
     * $instance object
     * @var null
     */
    private static $instance = null;

    private $session;

    private $db;

	/**
     * getInstance create or return object
     * @return object | this object
     */
    public static function getInstance()
    {
        if (self::$instance === null) {

            self::$instance = new self();

            self::$instance->session = Session::getInstance();

            self::$instance->db = Database::getInstance();
        }
        
        return self::$instance;
    }

    public function setSession()
    {

    	$this->session->setSession('logged');
    }

    public function logStatus()
    {
        if ($this->session->getSession() == 'logged') {

           return true;

        }else{

            return false;
        }

    }

    public function clearSession()
    {
        if ($this->session->getSession() == 'logged') {

            $this->session->clear();

        }

    }


	private function __clone() {}
    private function __construct() {}



}